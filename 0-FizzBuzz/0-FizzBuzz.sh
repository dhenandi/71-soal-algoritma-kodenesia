#!/bin/bash

for (( i=1; i<=100; i++ ))

do

        if [[ $(( $i % 3 )) == 0 && $(( $i % 5 )) == 0 ]]; then

        echo "$i FizzBuzz"

        else if [[ $(( $i % 3 )) == 0 ]]; then

        echo "$i Fizz"

        else if [[ $(( $i % 5 )) == 0 ]]; then

        echo "$i Buzz"

        else

        echo $i

        fi
        fi
        fi

done
